﻿using UnityEngine;
using System.Collections;

public class ApplicationManager : MonoBehaviour {
	
    public void Start()
    {
        #if UNITY_EDITOR
            GetComponent<FPSDisplay>().enabled = true;
        #else
            GetComponent<FPSDisplay>().enabled = false;
        #endif
    }

    public void Quit () 
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit();
		#endif
	}
}
