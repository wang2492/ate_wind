﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.PostProcessing;

public class GraphicsController : MonoBehaviour {

    public Dropdown presetDrowdown;
    public GraphicsPreset[] presets;

    public Slider distanceSlider;
    public Slider densitySlider;
    public Slider cameraRangeSlider;

    public Toggle ppeToggle;
    public Toggle treeToggle;

    private int grassDrawDistnace;
    private float grassDensity;

    public TerrainDistanceController tdc;

    private bool usePostProcessingEffects;
    public GameObject trees;
    public Camera cam;
    public PostProcessingBehaviour ppb;

    void Start() {
        OverrideSettings();   
    }

    void Awake() {
        OverrideSettings();
    }

    void OverrideSettings() {
        //GraphicsOverrideController gco=GameObject.FindGameObjectWithTag("GraphicsControllerOverride").GetComponent<GraphicsOverrideController>();
        distanceSlider.value=GraphicsOverrideController.grassDrawDistance;
        densitySlider.value = GraphicsOverrideController.grassDensity;
        ppeToggle.isOn = GraphicsOverrideController.usePostProcessingEffects;
        treeToggle.isOn = GraphicsOverrideController.renderTrees;
        cameraRangeSlider.value=GraphicsOverrideController.cameraRange;
        UpdateSettings();
    }


    public void UpdateSettings()
    {
        grassDrawDistnace = (int)distanceSlider.value;
        grassDensity = densitySlider.value;
        usePostProcessingEffects = ppeToggle.isOn;

        tdc.Distance = grassDrawDistnace;
        tdc.density = grassDensity;
        ppb.enabled = usePostProcessingEffects;
        trees.SetActive(treeToggle.isOn);
        cam.farClipPlane = cameraRangeSlider.value;

        GraphicsOverrideController.ApplyCustomSettings(grassDrawDistnace, grassDensity, usePostProcessingEffects, treeToggle.isOn, cameraRangeSlider.value);

    }

    public void UpdatePreset()
    {
        ApplyPreset(presetDrowdown.value);
    }

    public void ApplyPreset(int index)
    {
        GraphicsPreset p = presets[index];
        distanceSlider.value = p.grassDistance;
        densitySlider.value = p.grassDensity;
        ppeToggle.isOn = p.usePPE;
        treeToggle.isOn = p.RenderTrees;
        cameraRangeSlider.value = p.CameraRange;
    }


}

[System.Serializable]
public class GraphicsPreset
{
    public string name;
    public int grassDistance;
    public float grassDensity;
    public bool usePPE;
    public bool RenderTrees;
    public int CameraRange;
}
