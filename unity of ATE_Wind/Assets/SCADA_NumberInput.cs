﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SCADA_NumberInput : MonoBehaviour {

    public int turbineNumber;
    public InputField input;

    public void Update() {
        input.GetComponentInChildren<Text>().text = turbineNumber.ToString();
    }
  
}
