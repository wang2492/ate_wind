﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingBar : MonoBehaviour {

	public Image bar;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}


	public void LoadMainScene()
	{
		StartCoroutine (loadingMain());
	}

    public void LoadWindFarmScene() {
        StartCoroutine(loadingWindFarmScene());
    }

    public void LoadTBoxScene()
    {

        StartCoroutine(loadingTBox());

    }

    public void LoadSceneByIndex(int index) {
        StartCoroutine(loadingSceneByIndex(index));
    }


	IEnumerator loadingMain()
	{
		yield return new WaitForSeconds (1);
//		AsyncOperation ms = Application.LoadLevelAsync (1);
//		AsyncOperation ms = Application.LoadLevelAsync(1);
		AsyncOperation ms = SceneManager.LoadSceneAsync(1);
		while (!ms.isDone) 
		{
			bar.fillAmount = ms.progress  ;
			yield return null;
		}
	}

    IEnumerator loadingTBox()
    {
        yield return new WaitForSeconds(1);
        //		AsyncOperation ms = Application.LoadLevelAsync (1);
        //		AsyncOperation ms = Application.LoadLevelAsync(1);
        AsyncOperation ms = SceneManager.LoadSceneAsync(3);
        while (!ms.isDone)
        {
            bar.fillAmount = ms.progress;
            yield return null;
        }
    }

    IEnumerator loadingWindFarmScene() {
        yield return new WaitForSeconds(1);
        AsyncOperation ms = SceneManager.LoadSceneAsync(2);
        while (!ms.isDone) {
            bar.fillAmount = ms.progress;
            yield return null;
        }
    }

    IEnumerator loadingSceneByIndex(int i) {
        yield return new WaitForSeconds(1);
        AsyncOperation ms = SceneManager.LoadSceneAsync(i);
        while (!ms.isDone) {
            bar.fillAmount = ms.progress;
            yield return null;
        }
    }




















}
