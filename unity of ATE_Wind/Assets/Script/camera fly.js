var mainSpeed : float = 100.0; //regular speed

var shiftAdd : float = 250.0; //multiplied by how long shift is held.  Basically running

var maxShift : float = 1000.0; //Maximum speed when holdin gshift

var shiftSub : float = 5; //Slows down movement by a factor of 5

//var minShift : float = 25;// Minimum speed when holding Alt

var camSens : float = 0.25; //How sensitive it with mouse

private var lastMouse = Vector3(255, 255, 255); //kind of in the middle of the screen, rather than at the top (play)

private var totalRun : float  = 1.0;

 

function Update () {

    //Keyboard commands

    var f : float = 0.0;

    var p : Vector3 = Vector3(0.0, 0.0, 0.0);

    if (ControlGetBaseInput() == Vector3(0.0, 0.0, 0.0)){
    	
		p = GetBaseInput();

	}

	else {

		p = ControlGetBaseInput();

	}


    if (Input.GetKey(KeyCode.LeftShift) || Input.GetButton("Y Button")){

        totalRun += Time.deltaTime;

        p  = p * totalRun * shiftAdd;

        p.x = Mathf.Clamp(p.x, -maxShift, maxShift);

        p.y = Mathf.Clamp(p.y, -maxShift, maxShift);

        p.z = Mathf.Clamp(p.z, -maxShift, maxShift);

    }

    else if (Input.GetKey(KeyCode.LeftAlt) || Input.GetButton("B Button")){

        p  = p * mainSpeed / 5; //shiftSub seems to be broken, replaced with the actual value

        }

    	else{

        totalRun = Mathf.Clamp(totalRun * 0.5, 1, 1000);

        p = p * mainSpeed;

        }

    

   

    p = p * Time.deltaTime;

    if (Input.GetKey(KeyCode.Space)){ //If player wants to move on X and Z axis only

        f = transform.position.y;

        transform.Translate(p);

        transform.position.y = f;

    }

    else{

        transform.Translate( p);

    }

   

}

 

private function GetBaseInput() : Vector3 { //returns the basic values, if it's 0 than it's not active.

    var p_Velocity : Vector3;

    if (Input.GetKey (KeyCode.W)){

        p_Velocity += Vector3(0, 0 , 1);

    }
     if (Input.GetKey (KeyCode.E)){

        p_Velocity += Vector3(0, 1 , 0);

    }
    if (Input.GetKey (KeyCode.C)){

        p_Velocity += Vector3(0, -1 , 0);

    }

    if (Input.GetKey (KeyCode.S)){

        p_Velocity += Vector3(0, 0 , -1);

    }

    if (Input.GetKey (KeyCode.A)){

        p_Velocity += Vector3(-1, 0 , 0);

    }

    if (Input.GetKey (KeyCode.D)){

        p_Velocity += Vector3(1, 0 , 0);

    }

    return p_Velocity;

}

private function ControlGetBaseInput() : Vector3 { //returns the basic values, if it's 0 than it's not active.

	var elevation : float = 0.0;

	if (Input.GetButton("Right Bumper")){

     	elevation = 1;

    }
    else if (Input.GetButton("Left Bumper")){

        elevation = -1;

    }
    else {

    elevation = 0;

    }


    var c_Velocity : Vector3 = Vector3(Input.GetAxis("Horizontal Controller"), elevation, Input.GetAxis("Vertical Controller"));

    return c_Velocity;

}