﻿using UnityEngine;
using System.Collections;

public class CloseMenuCheck : MonoBehaviour {

	private GameObject lastMenu;
	public GameObject menuToClose;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		InteractivityCheck A = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<InteractivityCheck> ();

			if (A.GetLastActiveMenuBool () == false) {

				menuToClose.SetActive (false);


			}


			}
}
