﻿using UnityEngine;
using System.Collections;

public class Mouse : MonoBehaviour {


	// Use this for initialization
	void OnMouseEnter()
	{
		//Entered = true;


			if (gameObject.GetComponent<Renderer>() != null)
			{
				gameObject.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
				gameObject.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(1, 0.45f, 0.1f));
			}
			if (gameObject.GetComponentsInChildren<MeshRenderer>()!=null)
			{
				MeshRenderer[] render=gameObject.GetComponentsInChildren<MeshRenderer>();
				foreach (MeshRenderer r in render){
					Material[] mat=r.materials;
					foreach (Material m in mat){
						m.EnableKeyword ("_EMISSION");
						m.SetColor("_EmissionColor", new Color(0.5f,0.5f,0.5f));
					}
				}
				/*Material[] mat=gameObject.GetComponentInChildren<Renderer>().materials;
				foreach (Material m in mat){
					m.EnableKeyword ("_EMISSION");
					m.SetColor("_EmissionColor", new Color(1, 0.45f, 0f));
				}*/
				//gameObject.GetComponentInChildren<Renderer>().material.EnableKeyword ("_EMISSION");
				//gameObject.GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", new Color(1, 0.45f, 0f));

		}
	}
	void OnMouseExit()
	{
	
		
			if (gameObject.GetComponent<Renderer> () != null) {
				gameObject.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", Color.black);
			} 
			if (gameObject.GetComponentsInChildren<MeshRenderer>()!=null){
				MeshRenderer[] render=gameObject.GetComponentsInChildren<MeshRenderer>();
				foreach (MeshRenderer r in render){
						Material[] mat=r.materials;
					foreach (Material m in mat){
						m.EnableKeyword ("_EMISSION");
						m.SetColor("_EmissionColor", Color.black);
					}
				}
				//GetComponentInChildren<Renderer> ().material.EnableKeyword ("_EMISSION");
				//GetComponentInChildren<Renderer> ().material.SetColor ("_EmissionColor", Color.black);
			}

	}


	/*void OnGUI(){
		if (Entered) {
			GUI.DrawTexture (new Rect (Event.current.mousePosition.x - 15, Event.current.mousePosition.y - 15, 64, 64), hand);
			Cursor.visible = false;
		} else {
			Cursor.visible = true;
		}
	}*/
}
