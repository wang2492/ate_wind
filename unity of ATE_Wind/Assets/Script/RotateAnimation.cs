﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAnimation : MonoBehaviour {

    public Vector3 speed;
    public Vector3 randomStartOffset;

    void Start()
    {
        Vector3 startOffset = new Vector3(Random.Range(0, randomStartOffset.x), Random.Range(0, randomStartOffset.y), Random.Range(0, randomStartOffset.z));
    }

    void Update()
    {
        transform.rotation=Quaternion.Euler(transform.rotation.eulerAngles+speed);
    }
}
