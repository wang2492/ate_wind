﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetManualModeOnEnable : MonoBehaviour {

    public enum ManualModeValue { KeepDefault, OverrideToTrue, OverrideToFalse  };

    public ManualModeValue ManualValueOnEnable;
    public ManualModeValue ManualValueOnDisable;
    public ManualModeValue ManualValueOnUpdate;

    public GameObject scriptholder;

    private void Start() {
        if (scriptholder == null) {
            scriptholder = GameObject.Find("Scriptholder");
        }
    }

    private void OnEnable() {
        switch (ManualValueOnEnable) {
            case ManualModeValue.OverrideToTrue:
                scriptholder.GetComponent<measurementAssignment>().SetManualMode(true);
                break;
            case ManualModeValue.OverrideToFalse:
                scriptholder.GetComponent<measurementAssignment>().SetManualMode(false);
                break;
        }  
    }

    private void OnDisable() {
        switch (ManualValueOnDisable) {
            case ManualModeValue.OverrideToTrue:
                scriptholder.GetComponent<measurementAssignment>().SetManualMode(true);
                break;
            case ManualModeValue.OverrideToFalse:
                scriptholder.GetComponent<measurementAssignment>().SetManualMode(false);
                break;
        }
    }

    private void Update() {
        switch (ManualValueOnUpdate) {
            case ManualModeValue.OverrideToTrue:
                scriptholder.GetComponent<measurementAssignment>().SetManualMode(true);
                break;
            case ManualModeValue.OverrideToFalse:
                scriptholder.GetComponent<measurementAssignment>().SetManualMode(false);
                break;
        }
    }




}
