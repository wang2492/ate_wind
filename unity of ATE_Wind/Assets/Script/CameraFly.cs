using UnityEngine;
using System.Collections;

	
public class CameraFly : MonoBehaviour {

	public float mainSpeed = 20;
	public float customSpeed;
	public float shiftAdd = 1;
	public float altSlow = 0.1f;
	public float maxShift = 1000;
	public float camSens = 0.25f;
//	private bool isMAC = false;
	private bool bButton = false;
	private bool yButton = false;
//	private bool lBumper = false;
//	private bool rBumper = false;
//	private float lStickXAxis = 0f;
//	private float lStickYAxis = 0f;

	//	private Vector3 lastMouse;
	//	private float totalRun = 1.0f;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

//		SetControllerToOS ();

		Vector3 p = new Vector3(0.0f, 0.0f, 0.0f);
        Vector3 q = new Vector3(0.0f, 0.0f, 0.0f);

        //		float f = 1.0f;



//        if (ControlGetBaseInput() == new Vector3(0.0f, 0.0f, 0.0f)){
//
			q = GetBaseInput();
        //
        //		}
        //
        //		else {
        //
        //			q = ControlGetBaseInput();
        //
        //		}



        if (Input.GetKey(KeyCode.LeftShift) || yButton) {

            //			totalRun += Time.deltaTime;
            //			p = p * totalRun * shiftAdd;
            p = q * mainSpeed * shiftAdd * Time.deltaTime;
            p.x = Mathf.Clamp(p.x, -maxShift, maxShift);
            p.y = Mathf.Clamp(p.y, -maxShift, maxShift);
            p.z = Mathf.Clamp(p.z, -maxShift, maxShift);
            transform.Translate(p);

        } else if (Input.GetKey(KeyCode.LeftAlt) || bButton) {

            p = q * mainSpeed * altSlow * Time.deltaTime;

            p.x = Mathf.Clamp(p.x, -maxShift, maxShift);
            p.y = Mathf.Clamp(p.y, -maxShift, maxShift);
            p.z = Mathf.Clamp(p.z, -maxShift, maxShift);
            transform.Translate(p);
            //Debug.Log("alt is pressed: " + p + "," + q);

        } else {
            //totalRun = Mathf.Clamp(totalRun * 0.5f, 1, 1000);
            //p = p * mainSpeed;
            //}

            transform.Translate(q);
        }

	}

	private Vector3 GetBaseInput(){

		Vector3 p_Velocity = new Vector3(0,0,0);

		if (Input.GetKey (KeyCode.W)) {
			p_Velocity += Vector3.forward * mainSpeed * Time.deltaTime;
			print ("W");
		}

		if (Input.GetKey (KeyCode.S)) {
			p_Velocity += Vector3.back * mainSpeed * Time.deltaTime;
		}

		if (Input.GetKey (KeyCode.A)) {
			p_Velocity += Vector3.left * mainSpeed * Time.deltaTime;
		}

		if (Input.GetKey (KeyCode.D)) {
			p_Velocity += Vector3.right * mainSpeed * Time.deltaTime;
		}

		if (Input.GetKey (KeyCode.E)) {
            //p_Velocity += Vector3.up * mainSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.LeftAlt)){
                transform.position += Vector3.up * mainSpeed * Time.deltaTime*altSlow;
            } else if (Input.GetKey(KeyCode.LeftShift)) {
                transform.position += Vector3.up * mainSpeed * Time.deltaTime* shiftAdd;
            } else {
                transform.position += Vector3.up * mainSpeed * Time.deltaTime;
            }
            
		}

		if (Input.GetKey (KeyCode.C)) {
            //p_Velocity += Vector3.down * mainSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                transform.position += Vector3.down * mainSpeed * Time.deltaTime * altSlow;
            }
            else if (Input.GetKey(KeyCode.LeftShift))
            {
                transform.position += Vector3.down * mainSpeed * Time.deltaTime * shiftAdd;
            }
            else
            {
                transform.position += Vector3.down * mainSpeed * Time.deltaTime;
            }
        }

		return p_Velocity;
	}

    public void AdjustSpeed(float customSpeed)
    {

        mainSpeed = customSpeed;


    }

    //	private Vector3 ControlGetBaseInput() { //returns the basic values, if it's 0 than it's not active.
    //
    //		float elevation = 0.0f;
    //
    //		if (rBumper){
    //
    //			elevation = 1f;
    //
    //		}
    //		else if (lBumper){
    //
    //			elevation = -1f;
    //
    //		}
    //		else {
    //
    //			elevation = 0.0f;
    //
    //		}
    //
    //		float x_Axis = lStickXAxis;
    //		float y_Axis = lStickYAxis;
    //
    //		Vector3 c_Velocity = new Vector3(x_Axis, elevation, y_Axis);
    //
    //		return c_Velocity;
    //
    //	}

    //	private void SetControllerToOS() { //sets all the controller inputs based on the OS
    //
    //		if (Application.platform == RuntimePlatform.OSXPlayer) {
    //
    //			isMAC = true;
    //
    //		}
    //
    //		if (isMAC == true) {
    //			yButton = Input.GetButton ("Y Button MAC");
    //			bButton = Input.GetButton ("B Button MAC");
    //			rBumper = Input.GetButton ("Right Bumper MAC");
    //			lBumper = Input.GetButton ("Left Bumper MAC");
    //			lStickXAxis = Input.GetAxis ("Horizontal Controller MAC");
    //			lStickYAxis = Input.GetAxis ("Vertical Controller MAC");
    //		} 
    //		else {
    //			yButton = Input.GetButton ("Y Button");
    //			bButton = Input.GetButton ("B Button");
    //			rBumper = Input.GetButton ("Right Bumper");
    //			lBumper = Input.GetButton ("Left Bumper");
    //			lStickXAxis = Input.GetAxis ("Horizontal Controller");
    //			lStickYAxis = Input.GetAxis ("Vertical Controller");
    //		}
    //	}

}
