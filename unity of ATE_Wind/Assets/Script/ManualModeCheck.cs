﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManualModeCheck : MonoBehaviour {

    [System.Serializable]
    public class ManualModePitchMotorController 
    {

        public bool manualModeEngaged = false;
        public Text[] manualModeTextObjects;
        public GameObject brakeResultsMMOn;
        public GameObject brakeResultsMMOff;
        public bool isFailing = false;
        public GameObject brakeResultsMMOnBroken;
        public GameObject scriptholder;

    }

    public ManualModePitchMotorController motorMenuController = new ManualModePitchMotorController();

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        motorMenuController.manualModeEngaged = motorMenuController.scriptholder.GetComponent<measurementAssignment>().isOnManual;


        foreach (Text a in motorMenuController.manualModeTextObjects)
        {

            if (motorMenuController.manualModeEngaged == true)
            {

                a.text = "On";

            } 
            else
            {

                a.text = "Off";

            }

        }

	}

    public void OnBrakeMeasurementClick()
    {

        if (motorMenuController.isFailing == true)
        {

            motorMenuController.brakeResultsMMOnBroken.SetActive(true);

        }

        else {

            if (motorMenuController.manualModeEngaged == false)
            {

                motorMenuController.brakeResultsMMOff.SetActive(true);

            }

            else
            {

                motorMenuController.brakeResultsMMOn.SetActive(true);

            }

        }
     

    }
}
