﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TeleportTo : MonoBehaviour {

    //the list of location points for teleporting (see class below)
    public TeleportLocation[] locations;

    //the object to be teleported
    public GameObject targetGameObject;

    public void Start() {
        //if no target object to teleport is assigned, the script will teleport itself
        if (targetGameObject == null) {
            targetGameObject = gameObject;
        }
    }


    //teleporting by coordinate values
    public void TeleportToLocation(Vector3 position, Vector3 rotation) {
        targetGameObject.transform.position = position;
        targetGameObject.transform.eulerAngles = rotation;
    }

    //teleporting based on array index of lcations variable
    public void TeleportToLocation(int index) {
        targetGameObject.transform.position = locations[index].position;
        targetGameObject.transform.eulerAngles = locations[index].rotation;
    }

    //teleporting based on location name
    public void TeleportToLocation(string name) {
        foreach (TeleportLocation location in locations) {
            if (location.name == name) {
                targetGameObject.transform.position = location.position;
                targetGameObject.transform.eulerAngles = location.rotation;
                return;
            }
        }
        Debug.Log("Location name not found");
        return;
    }

    //teleporting directly from a TeleportLocation variable
    public void TeleportToLocation(TeleportLocation location) {
        targetGameObject.transform.position = location.position;
        targetGameObject.transform.eulerAngles = location.rotation;
        
    }

    public void SetMouseLook(bool value) {
        if (targetGameObject.GetComponent<MouseLook>() == null) {
            return;
        }
        targetGameObject.GetComponent<MouseLook>().enabled = value;
    }


    //input handling for teleportation
    private void Update() {
        foreach (TeleportLocation location in locations) {
            if (Input.GetKey(location.key)){
                SetMouseLook(false);
                TeleportToLocation(location);
                SetMouseLook(true);
                targetGameObject.GetComponent<MouseLook>().OverrideRotation(location.rotation);
            }
        }
    }

}

//class used to store location and rotation info in one variable
[System.Serializable]
public class TeleportLocation {
    public string name;     //name of the location
    public Vector3 position;        //position to teleport to
    public Vector3 rotation;     //rotation to change to
    public KeyCode key;     //keycode to teleport to this location
}
