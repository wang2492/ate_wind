﻿using UnityEngine;
using System.Collections;

public class MeasurementHelp : MonoBehaviour {

	public GameObject measurementPanel;
	public GameObject helpPanel;
	public GameObject helpText;
	public GameObject mainCamera;
	public GameObject mainComponent;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if ((mainCamera.transform.position - mainComponent.transform.position).magnitude > 1) {

			measurementPanel.SetActive (false);
			helpPanel.SetActive (false);

		}

	}

	void OnMouseOver (){

		if ((mainCamera.transform.position - mainComponent.transform.position).magnitude <= 1) {

			helpPanel.SetActive (true);

			helpText.SetActive (true);

		}

		else {

			helpPanel.SetActive (false);
			helpText.SetActive (false);

		}
	}

	void OnMouseExit () {
	
		helpPanel.SetActive (false);
		helpText.SetActive (false);

	}

}
