﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuPanelInteractivityCheck : MonoBehaviour {

	public GameObject menuButton;
	public GameObject menuPanel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (menuButton.GetComponent<Button> ().IsInteractable () == false) {
		
			menuPanel.SetActive (false);

		}

	}
}
