﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseHoverText : MonoBehaviour {

    public Text text;
    public string message;
    public float distance;
    public GameObject camera;

    void OnMouseOver() {
        if(Vector3.Distance(transform.position, camera.transform.position) < distance){
        text.gameObject.SetActive(true);
        text.text = message;
        }
    }

    void OnMouseExit() {
        text.gameObject.SetActive(false);
    }

}
