﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuInteractivityToggle : MonoBehaviour {

	public GameObject toggleMenu;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void MenuToggle () {
	
		if (toggleMenu.GetComponent<Button>().IsInteractable () == true) {

			toggleMenu.GetComponent<Button>().interactable = false;

		} else {

			toggleMenu.GetComponent<Button>().interactable = true;

		}
	
	
	}
}
