﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sheets : MonoBehaviour {

    public Sprite[] sheetImages;
    public Image currentSheetImage;
    public Text currentSheetNumberText;
    public Button nextButton;
    public Button backButton;
    public Button zoomIn;
    public Button zoomOut;

    private int sheetsLength;
    private int zoomAmount;
    private int currentSheetNumber;
    private bool zoomChanged = false;
    private bool pageChanged = false;

	// Use this for initialization
	void Awake () {

        sheetsLength = sheetImages.Length;

        for (int i = 0; i < (sheetsLength - 1); i++) {

            zoomAmount = 1;

        }

        currentSheetNumber = 1;

        Debug.Log("Initialization Complete");
		
	}
	
	// Update is called once per frame
	void Update () {

		
        if (zoomChanged == true)
        {

            ZoomAmountCheck();

        }

        if (pageChanged == true)
        {

            WritePageNumber();

            pageChanged = false;

        }

        BackButton();
        NextButton();
        Zoom();
        Unzoom();

	}

    public void NextButtonOnClick()
    {

        zoomAmount = 1;
        zoomChanged = true;

        if(currentSheetNumber==sheetsLength) {
            currentSheetNumber = 1;
        }else {
            currentSheetNumber = currentSheetNumber+1;
        }
        currentSheetImage.sprite = sheetImages[currentSheetNumber - 1];
        pageChanged = true;
        Debug.Log("Working");
        Debug.Log(currentSheetNumber);



    }

    public void NextButton()
    {

        if (currentSheetNumber == sheetsLength)
        {

            //nextButton.interactable = false;

        }
        else
        {

            //nextButton.interactable = true;

        }

    }

    public void BackButtonOnclick()
    {

        zoomAmount = 1;
        zoomChanged = true;

        if(currentSheetNumber==1) {
            currentSheetNumber = sheetsLength;
        } else {
            currentSheetNumber = currentSheetNumber - 1;
        }
        
        currentSheetImage.sprite = sheetImages[currentSheetNumber - 1];
        pageChanged = true;
        Debug.Log("Working");
        Debug.Log(currentSheetNumber);

    }

    public void BackButton()
    {

        if (currentSheetNumber == 1)
        {

            //backButton.interactable = false;

        }
        else
        {

            //backButton.interactable = true;

        }


    }

    public void ZoomOnClick()
    {

        zoomAmount = zoomAmount+1;
        zoomChanged = true;
        Debug.Log("Working");


    }

    public void Zoom()
    {

        if (zoomAmount == 3)
        {

            zoomIn.interactable = false;

        }
        else
        {

            zoomIn.interactable = true;

        }


    }

    public void UnzoomOnClick()
    {

        zoomAmount = zoomAmount-1;
        zoomChanged = true;
        Debug.Log("Working");

    }

    public void Unzoom()
    {

        if (zoomAmount == 1)
        {

            zoomOut.interactable = false;

        } else
        {

            zoomOut.interactable = true;

        }


    }

    private void ZoomAmountCheck()
    {

        if (zoomAmount == 3)
        {

            currentSheetImage.rectTransform.sizeDelta = new Vector2(1536f, 959.25f);

        }

        if (zoomAmount == 2)
        {

          
            currentSheetImage.rectTransform.sizeDelta = new Vector2(1024f, 639.5f);

        }
        if (zoomAmount == 1)
        {

            currentSheetImage.rectTransform.sizeDelta = new Vector2(720f, 450f);

        }

        zoomChanged = false;
        Debug.Log("Triggered");

    }

    public void WritePageNumber()
    {

        currentSheetNumberText.text = (currentSheetNumber.ToString() + "/" + sheetsLength);

    }


}
