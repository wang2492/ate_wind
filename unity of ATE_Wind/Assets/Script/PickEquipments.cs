﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickEquipments : MonoBehaviour {

	public GameObject ComputerScreen_Panel;

	public GameObject SafetySheet_Panel;
	public GameObject _SafetySheet;
	public GameObject Pickup_Panel;
	public GameObject Pickup_Panel_Image;
	public GameObject Pickup_Panel_InspectText;
	public GameObject _Earplug;
	public GameObject _Gloves;
	public GameObject _Multimeter;
	public GameObject _N_R_Lanyard;
	public GameObject _Lanyard;
	public GameObject _Harness;
	public GameObject _Glasses;
	public bool B_Earplug;
	public bool B_Gloves;
	public bool B_Multimeter;
	public bool B_N_R_Lanyard;
	public bool B_Lanyard;
	public bool B_Harness;
	public bool B_Glasses;
	public Sprite[] Equipment_Image;

	//For inventory system
	public const int TOTAL =12;
	public int x = 0;
	public int c = 0;
	public GameObject[] slots = new GameObject[TOTAL];
	public GameObject SlotPanel;
	public List<bool> slotList;
	//public GameObject 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast (ray, out  hit, 1000)) {
			if (Input.GetMouseButtonDown (0)) 
			{
				
				Pickup_Panel_InspectText.SetActive (false);
				if (hit.transform.tag == "EarPlug") 
				{
					B_Earplug = true;
					Pickup_Panel.SetActive (true);
					_Earplug.SetActive (false);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[0];
				}


				if (hit.transform.tag == "Harness") 
				{
					B_Harness = true;
					Pickup_Panel.SetActive (true);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[1];
					_Harness.SetActive (false);
				}

				if (hit.transform.tag == "Glasses") 
				{
					B_Glasses = true;
					Pickup_Panel.SetActive (true);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[2];
					_Glasses.SetActive (false);

				}
				if (hit.transform.tag == "Gloves") 
				{
					B_Gloves = true;
					Pickup_Panel.SetActive (true);
					_Gloves.SetActive (false);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[3];
				}

				if (hit.transform.tag == "Multimeter") 
				{
					B_Multimeter = true;
					Pickup_Panel.SetActive (true);
					_Multimeter.SetActive (false);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[4];
				}
				if (hit.transform.tag == "N_R_Lanyard") 
				{
					B_N_R_Lanyard = true;
					_N_R_Lanyard.SetActive (false);
					Pickup_Panel.SetActive (true);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[5];
				}

				if(hit.transform.tag=="Lanyard")
				{
					B_Lanyard=true;
					Pickup_Panel.SetActive(true);
					_Lanyard.SetActive(false);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[6];
				}

				if(hit.transform.tag=="HubSafetySheet")
				{
					SafetySheet_Panel.SetActive(true);
				}
			
				if(hit.transform.tag=="ComputerScreen")
				{
					ComputerScreen_Panel.SetActive(true);
				}

			}
		}
			
	}


	public void SetActive(GameObject g)
	{
		if (g.activeSelf) {
			g.SetActive (false);
		} else 
		{
			g.SetActive(true);
		}
	}

	public void Cancel()
	{
		Pickup_Panel.SetActive (false);
		if (B_Earplug) 
		{
			_Earplug.SetActive (true);
			B_Earplug = false;
		}

		if (B_Gloves) 
		{
			_Gloves.SetActive (true);
			B_Gloves = false;
		}
		if (B_Multimeter) 
		{
			_Multimeter.SetActive (true);
			B_Multimeter = false;
		}
		if (B_N_R_Lanyard) 
		{
			_N_R_Lanyard.SetActive (true);
			B_N_R_Lanyard = false;

		}

		if (B_Harness)
		{
			_Harness.SetActive (true);
			B_Harness = false;
		}
		if (B_Glasses) 
		{
			_Glasses.SetActive (true);
			B_Glasses = false;
		}
	}

	public void Pick_Yes()
	{
		Pickup_Panel.SetActive (false);
		if (B_Earplug) 
		{
			_Earplug.SetActive (false);
			B_Earplug = false;
			x=0;
		}

		if (B_Gloves) 
		{
			_Gloves.SetActive (false);
			B_Gloves = false;
			x=3;
		}
		if (B_Multimeter) 
		{
			_Multimeter.SetActive (false);
			B_Multimeter = false;
			x=4;
		}
		if (B_N_R_Lanyard) 
		{
			_N_R_Lanyard.SetActive (false);
			B_N_R_Lanyard = false;
			x=5;
		}

		if (B_Harness)
		{
			_Harness.SetActive(false);
			B_Harness = false;
			x=1;
		}
		if (B_Glasses) 
		{
			_Glasses.SetActive (false);
			B_Glasses = false;
			x=2;
		}
		if (B_Lanyard) 
		{
			_Lanyard.SetActive(false);
			B_Lanyard=false;
			x=6;
		}


	}

	public void Pick_Inspect()
	{
		Pickup_Panel_InspectText.SetActive (true);
	}

	public void AddtoInventory()
	{
		if (slotList.Contains (false)) {
			c = slotList.IndexOf(false);
		}else{
			c = slotList.Count;
			slotList.Add (true);
			
		} 
		slots[c].GetComponent<Image>().sprite =Equipment_Image[x];
	}



}
