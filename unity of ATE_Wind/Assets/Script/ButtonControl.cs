﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ButtonControl : MonoBehaviour {

	public GameObject subPanel;

	GameObject[] lv1panel;
	GameObject[] lv2panel;
	GameObject[] lv3panel;
	GameObject[] lv1button;
	GameObject[] lv2button;
	GameObject[] lv3button;

	GameObject controlButton;
	GameObject controlPanel;
	GameObject menuPanel;



	void Start () {	

	}

	// Update is called once per frame
	void Update () {

	}




	public void Setactive(){	

		lv1panel = GameObject.FindGameObjectsWithTag ("lv1Panel");
		lv2panel = GameObject.FindGameObjectsWithTag ("lv2Panel");
		lv3panel = GameObject.FindGameObjectsWithTag ("lv3Panel");
		lv1button = GameObject.FindGameObjectsWithTag ("lv1Button");
		lv2button = GameObject.FindGameObjectsWithTag ("lv2Button");

		if (!subPanel.activeSelf) {

			if (gameObject.tag == "lv2Button") {                                          // if Nacelle/Hub/Blades/Tower/Avatar button is clicked,set others to whith and close their sub panels
				
				foreach (GameObject lv2bt in lv2button) {
					lv2bt.GetComponent<Image> ().color = Color.white;
				}
				foreach (GameObject lv3pl in lv3panel) {
					lv3pl.SetActive (false);
				}
			}

			subPanel.SetActive (true);
			gameObject.GetComponent<Image> ().color = Color.gray;
		
		} else if (gameObject.name == "Menu_Button") {                                 // if Menu button is clicked

			foreach (GameObject lv1pl in lv1panel) {
				lv1pl.SetActive (false);
			}
			foreach (GameObject lv2pl in lv2panel) {
				lv2pl.SetActive (false);
			}
			foreach (GameObject lv3pl in lv3panel) {
				lv3pl.SetActive (false);
			}
			foreach (GameObject lv1bt in lv1button) {
				lv1bt.GetComponent<Image> ().color = Color.white;
			}
			foreach (GameObject lv2bt in lv2button) {
				lv2bt.GetComponent<Image> ().color = Color.white;
			}
			gameObject.GetComponent<Image> ().color = Color.white;
			print ("Menu_Button clicked");
		

		} else if (gameObject.name == "Components_Button") {                          // if Components button is clicked
			

			foreach (GameObject lv2pl in lv2panel) {
				lv2pl.SetActive (false);
			}
			foreach (GameObject lv3pl in lv3panel) {
				lv3pl.SetActive (false);
			}
			foreach (GameObject lv2bt in lv2button) {
				lv2bt.GetComponent<Image> ().color = Color.white;
			}
			gameObject.GetComponent<Image> ().color = Color.white;

		} else if (gameObject.tag == "lv3Button") {

			foreach (GameObject lv3pl in lv3panel) {
				lv3pl.SetActive (false);
			}
			foreach (GameObject lv2bt in lv2button) {
				lv2bt.GetComponent<Image> ().color = Color.white;
			}
		
		} 

		else {			                                                        
			    gameObject.GetComponent<Image> ().color = Color.white;
				subPanel.SetActive (false);
		}
		
	}


}

