﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


//Code that controls which probes control which probe png are active during measurement
//Attach this code to a controller and assign a high and low probe to it

public class ProbePopup : MonoBehaviour {

	public GameObject highProbe;
	public GameObject lowProbe;

	private bool highPressed;
	private bool lowPressed;
	private bool highMeasurementIsTakenOld = false;
	private bool highMeasurementIsTakenNew;
	private bool lowMeasurementIsTakenOld = false;
	private bool lowMeasurementIsTakenNew;
	private bool highChangeOffToOn = false;
	private bool lowChangeOffToOn = false;
	private bool highChangeOnToOff = false;
	private bool lowChangeOnToOff = false;
	private GameObject[] redProbes;
	private GameObject[] blackProbes;
	private GameObject[] buttonControllers;
	//private bool highMeasurementTaken;
	//private bool lowMeasurementTaken;
	//private bool particularButtonPressed;
	//private bool lowLocationPressed;
	//private bool highLocationPressed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		CheckIfMeasurementIsTaken ();
		DetectChangesInState ();
		UpdateMeasurementOldBools ();
		CheckForReset ();

		if (highMeasurementIsTakenOld == false) {

			foreach (GameObject a in redProbes) {

				a.SetActive (false);

			}

		}

		if (lowMeasurementIsTakenOld == false) {

			foreach (GameObject a in blackProbes) {

				a.SetActive (false);

			}

		}

	
	}

	private void CheckIfMeasurementIsTaken () {

		highMeasurementIsTakenNew = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetHighMeasurementTakenBool ();
		lowMeasurementIsTakenNew = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetLowMeasurementTakenBool ();

	}

	private void UpdateMeasurementOldBools () {

		highMeasurementIsTakenOld = highMeasurementIsTakenNew;
		lowMeasurementIsTakenOld = lowMeasurementIsTakenNew;

	}

	private void DetectChangesInState () {
		

			
			if ((highMeasurementIsTakenNew == true) & (highMeasurementIsTakenOld == false)) {

				highChangeOffToOn = true;

			}

			if ((highMeasurementIsTakenNew == false) & (highMeasurementIsTakenOld == true)) {

				highChangeOnToOff = true;

			}

			if ((lowMeasurementIsTakenNew == true) & (lowMeasurementIsTakenOld == false)) {

				lowChangeOffToOn = true;

			}

			if ((highMeasurementIsTakenNew == false) & (highMeasurementIsTakenOld == true)) {

				lowChangeOffToOn = true;

			}

	}

	public void ResetChangesInState () {

		highChangeOffToOn = false;
		lowChangeOffToOn = false;
		highChangeOnToOff = false;
		lowChangeOnToOff = false;

	}
		

	private void PullActiveProbePictures () {

		redProbes = GameObject.FindGameObjectsWithTag ("FloatingRedProbes");
		blackProbes = GameObject.FindGameObjectsWithTag ("FloatingBlackProbes");

	}
		
	private void ToggleCorrectProbes () {

		//This Section Turns on Correct Probes When Correct inputs are pressed
		if (lowChangeOffToOn == true) {

			foreach (GameObject a in blackProbes) {

				a.SetActive (false);

			}

			lowProbe.SetActive (true);

			lowChangeOffToOn = false;
			Debug.Log (lowProbe);
			Debug.Log (this.gameObject);
		}

		if (highChangeOffToOn == true) {

			foreach (GameObject a in redProbes) {

				a.SetActive (false);

			}

			highProbe.SetActive (true);

			highChangeOffToOn = false;
			Debug.Log (highProbe);
			Debug.Log (this.gameObject);
		}

		this.ResetChangesInState ();

	}

	private void CheckForReset () {

		PullActiveProbePictures ();

		if (lowChangeOnToOff == true) {

			foreach (GameObject a in blackProbes) {

				a.SetActive (false);

			}

			lowChangeOnToOff = false;

		}

		if (highChangeOnToOff == true) {

			foreach (GameObject a in redProbes) {

				a.SetActive (false);

			}

			highChangeOnToOff = false;
		}

	} 

	public void OnClickUpdate () {


		StartCoroutine (WaitingUpdateFunction ());

	}

	IEnumerator WaitingUpdateFunction () {

		yield return new WaitForEndOfFrame ();
	
		PullActiveProbePictures ();
		CheckForNewMeasurement ();
		ToggleCorrectProbes ();
		//CheckIfMeasurementIsTaken ();
		//ResetChangesInState ();
		//UpdateMeasurementOldBools ();

		buttonControllers = GameObject.FindGameObjectsWithTag ("ProbePictureControllers");

		foreach (GameObject a in buttonControllers) {

			a.GetComponent<ProbePopup> ().ResetChangesInState ();


		}


			

	}

	private void CheckForNewMeasurement () {

		if (GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetNewHighMeasurementTakenBool ()) {

			highChangeOffToOn = true;

		}

		if (GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetNewLowMeasurementTakenBool ()) {

			lowChangeOffToOn = true;

		}

		GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().ResetNewMeasurements ();

		if (highChangeOffToOn == true || lowChangeOffToOn == true) {
			Debug.Log ("no error in logic");
		}
	}

	//private void CheckMultimeterBools () {

	//highMeasurementTaken = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetHighMeasurementTakenBool ();
	//lowMeasurementTaken = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetLowMeasurementTakenBool ();
	//highPressed = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetHighMeasurementBool ();
	//lowPressed = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetLowMeasurementBool ();
	//highLocationPressed = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetHighLocationPressed ();
	//lowLocationPressed = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetLowLocationPressed ();

	//}
}
