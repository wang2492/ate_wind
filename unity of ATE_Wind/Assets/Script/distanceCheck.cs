﻿using UnityEngine;
using System.Collections;

public class distanceCheck : MonoBehaviour {

	public float distanceBetweenObjects = 1f;
	public GameObject object1;
	public GameObject object2;
	public GameObject menuToClose;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if ((object1.transform.position - object2.transform.position).magnitude > distanceBetweenObjects) {

			menuToClose.SetActive (false);
			GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<MeasurementModeSelection> ().ResetMeasurements ();

		}


	}
}
