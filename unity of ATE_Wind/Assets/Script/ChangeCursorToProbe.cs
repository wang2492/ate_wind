﻿using UnityEngine;
using System.Collections;

public class ChangeCursorToProbe : MonoBehaviour {

	public Texture2D blackProbeCursor;
	public Texture2D redProbeCursor;
	public CursorMode cursorMode = CursorMode.Auto;
	public Vector2 hotSpotHigh = Vector2.zero;
	public Vector2 hotSpotLow = Vector2.zero;

	private bool redProbeIsSelected = false;
	private bool blackProbeIsSelected = false;
	//private bool highMeasurementIsTaken = false;
	//private bool lowMeasurementIsTaken = false;
	private bool redProbeCursorOn = false;
	private bool blackProbeCursorOn = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		CheckProbes ();

		if (redProbeIsSelected == true) {

			blackProbeCursorOn = false;
			redProbeCursorOn = true;
			SetCursorToRedProbe ();

		} else if (blackProbeIsSelected == true) {

			blackProbeCursorOn = true;
			redProbeCursorOn = false;
			SetCursorToBlackProbe ();

		} else {

			blackProbeCursorOn = false;
			redProbeCursorOn = false;
			ResetCursor ();

		}

	}

	private void CheckProbes (){

		redProbeIsSelected = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetHighMeasurementBool ();
		blackProbeIsSelected = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetLowMeasurementBool ();
		//highMeasurementIsTaken = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetHighMeasurementTakenBool ();
		//lowMeasurementIsTaken = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ().GetLowMeasurementTakenBool ();

	}

	private void ToggleRedProbeCursor (){

		redProbeCursorOn = !redProbeCursorOn;

	}

	private void ToggleBlackProbeCursor (){

		blackProbeCursorOn = !blackProbeCursorOn;

	}

	public void SetRedProbeCursorOn (bool a) {

		redProbeCursorOn = a;

	}

	public void SetBlackProbeCursorOn (bool a) {

		blackProbeCursorOn = a;

	}

	public bool GetRedProbeCursorOn () {

		return redProbeCursorOn;

	}

	public bool GetBlackProbeCursorOn () {

		return blackProbeCursorOn;

	}

	public void SetCursorToRedProbe () {

		Cursor.SetCursor (redProbeCursor, hotSpotHigh, cursorMode);

	}

	public void SetCursorToBlackProbe () {

		Cursor.SetCursor (blackProbeCursor, hotSpotLow, cursorMode);

	}

	public void ResetCursor () {

		Cursor.SetCursor (null, Vector2.zero, cursorMode);

	}
}
