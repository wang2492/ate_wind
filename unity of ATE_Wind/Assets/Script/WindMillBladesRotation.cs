﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindMillBladesRotation : MonoBehaviour {

	public Transform m_windMillBlades;
	public float m_windMillBladesRotationSpeed = 25.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// FixedUpdate is called every second.
	void FixedUpdate() {
		m_windMillBlades.Rotate (0, 0, m_windMillBladesRotationSpeed * Time.deltaTime);
	}
}
