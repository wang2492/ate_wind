﻿using UnityEngine;
using System.Collections;

public class MenuDeactivate : MonoBehaviour {

	public void setactive(GameObject g)
	{
		if (g.activeSelf == true)
		{
			g.SetActive(false);
		}
	}
}