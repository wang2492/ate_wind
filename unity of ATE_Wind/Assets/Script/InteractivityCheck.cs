﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InteractivityCheck : MonoBehaviour {

	private GameObject lastActiveMenu;
	public GameObject toggleMenu;


	// Use this for initialization

	void Start () {
		
		lastActiveMenu = GameObject.Find ("Menu_Button");

	}
	
	// Update is called once per frame

	void Update () {

		if (lastActiveMenu.activeSelf == false) {

			toggleMenu.GetComponent<Button>().interactable = true;


		}

	}

	public void SetLastActiveMenu (GameObject a){
	
		lastActiveMenu = a;
	
	}

	public bool GetLastActiveMenuBool (){


		bool b = lastActiveMenu.activeSelf;
		return b;

	}
}
