﻿using UnityEngine;
using System.Collections;

public class MoveUpDown : MonoBehaviour {

	public int movespeed;
	public GameObject Avatar;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKey (KeyCode.E)) {
			Avatar.transform.position += Vector3.up * movespeed * Time.deltaTime;
		} else if (Input.GetKey (KeyCode.C)) 
		{
			Avatar.transform.position+=-Vector3.up*movespeed*Time.deltaTime;
		}

	}
}
