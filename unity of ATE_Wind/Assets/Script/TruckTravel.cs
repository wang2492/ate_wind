﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TruckTravel : MonoBehaviour {

    public int mainSceneIndex;

    public float distance;
    public GameObject camera;

    public LoadingBar loader;
    public GameObject loadingBarImage;

    private bool isHovering = false;

    void Update() {
        if (Input.GetMouseButtonDown(0) && isHovering && Vector3.Distance(transform.position,camera.transform.position)<distance) {
            loader.LoadWindFarmScene();
            loadingBarImage.SetActive(true);
        }
    }

    void OnMouseOver() {
        isHovering = true;
    }

    void OnMouseStay() {
        isHovering = true;
    }

    void OnMouseExit() {
        isHovering = false;
    }
}
