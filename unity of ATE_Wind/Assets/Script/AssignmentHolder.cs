﻿using UnityEngine;
using System.Collections;

public class AssignmentHolder : MonoBehaviour {

	private int currentObjectAssignment = 0;
	private int currentComponentAssignment = 0;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetCurrentObjectAssignment (int a){

		currentObjectAssignment = a;

	}

	public void SetCurrentComponentAssignment (int b){
	
		currentComponentAssignment = b;
	
	}

	public void SetCurrentAssignment (int objectAssignment, int componentAssignment){

		currentObjectAssignment = objectAssignment;
		currentComponentAssignment = componentAssignment;

	}

	public int GetCurrentObjectAssignment (){

		return currentObjectAssignment;

	}

	public int GetCurrentComponentAssignment() {
	
		return currentComponentAssignment;
	
	}


}
