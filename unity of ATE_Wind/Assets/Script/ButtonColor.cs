﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonColor : MonoBehaviour {
    public GameObject NacelleButton;
    public GameObject HubButton;
    public GameObject BladesButton;
    public GameObject TowerButton;
    public GameObject AvatarButton;
    public GameObject ComponentsButton;


    public GameObject ControlButton;
    public GameObject SheetButton;

    public void resetColor()
    {
        NacelleButton.GetComponent<Image>().color = Color.white;
        HubButton.GetComponent<Image>().color = Color.white;
        BladesButton.GetComponent<Image>().color = Color.white;
        TowerButton.GetComponent<Image>().color = Color.white;
        AvatarButton.GetComponent<Image>().color = Color.white;
        ComponentsButton.GetComponent<Image>().color = Color.white;
    }
    public void resetControlColor()
    {
        ControlButton.GetComponent<Image>().color = Color.white;
    }
    public void resetSheetColor()
    {
        SheetButton.GetComponent<Image>().color = Color.white;
    }
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
