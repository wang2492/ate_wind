﻿using UnityEngine;
using System.Collections;

public class Changelight : MonoBehaviour {
	public GameObject PowerSwitch;
	public GameObject Door;
	public GameObject LightSwitch;
    public GameObject switchOriginal;
    public GameObject switchDuplicated;


	public bool powerison = false;
	public bool doorisopen = false;
	public bool switchison = false;
	public bool check = false;
    public bool bulblight = false;
    public bool runlight = true;

    [System.Serializable]
    public class TBErrors
    {

        public bool doorWorking = true;
        public bool switchWorking = true;
        public bool connectionsWorking = true;

    }

    public TBErrors TBErrorList;

    public Material[] color;
 //   Renderer rend;


    
	// Use this for initialization
	void Start () {

    }



	// Update is called once per frame
	void Update () 
	{

        if (Input.GetMouseButtonDown (0)) 
		{
			print ("mouse left hit");
//			print (Camera.current);
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;


			if (Physics.Raycast (ray, out hit, 8)) 
			{		
				if (hit.collider.name == "PowerSwitch") {		

					powerison = !powerison;
				}

				if (hit.collider.name == "Door" ) {
					
					doorisopen = !doorisopen;
	         		check  = !check;
				} 
					
				if (hit.collider.name == "LightSwitch" ) {

					switchison = !switchison;
				}
                                
                if (hit.collider.name == "SwitchOriginal")
                {
                    runlight = !runlight;
                    switchOriginal.SetActive(false);
                    switchDuplicated.SetActive(true);

                }
                if (hit.collider.name == "SwitchDuplicated")
                {
                    runlight = !runlight;
                    switchDuplicated.SetActive(false);
                    switchOriginal.SetActive(true);
                }


            }

		
			if (powerison == true) {
				GameObject.Find ("PowerSwitch").transform.rotation = Quaternion.Euler (-90, 0, 0);
			} else {
				GameObject.Find ("PowerSwitch").transform.rotation = Quaternion.Euler (-90, 0, 180);
			}
				

			if (doorisopen == true) {
				GameObject.Find ("Door").transform.rotation = Quaternion.Euler (300, -90, 180);
			} else {
				GameObject.Find ("Door").transform.rotation = Quaternion.Euler (-90, -90, 180);
			}


			if (switchison == true) {
				GameObject.Find ("LightSwitch").transform.rotation = Quaternion.Euler (-99, -90, -90);
			} else {
				GameObject.Find ("LightSwitch").transform.rotation = Quaternion.Euler (-81, -90, -90);
			}



			//if (runlight == true)
            //{
            //    GameObject.Find("Switch").transform.rotation = Quaternion.Euler(-90, 180, 0);
            //    GameObject.Find("Switch").transform.position = new Vector3(-0.021175f, 0.337773f, 0.240347f);
            //}
            //else{
            //    GameObject.Find("Switch").transform.rotation = Quaternion.Euler(-90, 0, 0);
            //    GameObject.Find("Switch").transform.position = new Vector3(-0.021175f, 0.337773f, 0.24522f);
            //}





            if (powerison == true) {

				if (switchison == false) {

					if (doorisopen == true && TBErrorList.doorWorking == true) {

                        if (TBErrorList.connectionsWorking ==true)
                        {
                            GameObject.Find("bulb").GetComponent<Renderer>().material = color[1];
                        }
                        
						GameObject.Find ("RedLight").GetComponent<Renderer> ().material = color [2];
                        check = true;
					} else {
                        GameObject.Find("bulb").GetComponent<Renderer>().material = color[0];
						GameObject.Find ("RedLight").GetComponent<Renderer> ().material = color [3];
                    }
					if (check == true) {
						if (doorisopen == false && TBErrorList.switchWorking == true) {

                            if (TBErrorList.connectionsWorking == true)
                            {
                                GameObject.Find("bulb").GetComponent<Renderer>().material = color[1];
                            }

                            GameObject.Find ("RedLight").GetComponent<Renderer> ().material = color [2];
//						Debug.Log ("Lets wait 10 seconds");
                            StartCoroutine (TimeDelay ());
                            //						Debug.Log ("you should wait 10!!!! seconds");

                        } else {

                            if (TBErrorList.connectionsWorking == true)
                            {
                                GameObject.Find("bulb").GetComponent<Renderer>().material = color[1];
                            }

                            GameObject.Find ("RedLight").GetComponent<Renderer> ().material = color [2];
                        }
					}
				} else {
					check = true;

                    if (TBErrorList.connectionsWorking == true)
                    {
                        GameObject.Find("bulb").GetComponent<Renderer>().material = color[1];
                    }

                    GameObject.Find ("RedLight").GetComponent<Renderer> ().material = color [2];
                }
				
			} else {
                GameObject.Find("bulb").GetComponent<Renderer>().material = color[0];
				GameObject.Find ("RedLight").GetComponent<Renderer> ().material = color [3];
            }
            //					Debug.Log("a");

			if (runlight == false) {
				GameObject.Find("bulb").GetComponent<Renderer>().material = color[0];
				GameObject.Find ("RedLight").GetComponent<Renderer> ().material = color [3];
			}

//			if (GameObject.Find ("bulb").GetComponent<Renderer> ().material = color [1]) {
//				GameObject.Find ("RedLight").GetComponent<Renderer> ().material = color [2];
//			} else {
//				GameObject.Find ("RedLight").GetComponent<Renderer> ().material = color [0];
//			}
		}
	}




	IEnumerator TimeDelay() {
		yield return new WaitForSeconds(10);
        GameObject.Find("bulb").GetComponent<Renderer>().material = color[0];
		GameObject.Find ("RedLight").GetComponent<Renderer> ().material = color [3];
    }
}
