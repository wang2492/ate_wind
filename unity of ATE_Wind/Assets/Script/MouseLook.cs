using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// MouseLook rotates the transform based on the mouse delta.
/// Minimum and Maximum values can be used to constrain the possible rotation

/// To make an FPS style character:
/// - Create a capsule.
/// - Add the MouseLook script to the capsule.
///   -> Set the mouse look to use LookX. (You want to only turn character but not tilt it)
/// - Add FPSInputController script to the capsule
///   -> A CharacterMotor and a CharacterController component will be automatically added.

/// - Create a camera. Make the camera a child of the capsule. Reset it's transform.
/// - Add a MouseLook script to the camera.
///   -> Set the mouse look to use LookY. (You want the camera to tilt up and down like a head. The character already turns.)
[AddComponentMenu("Camera-Control/Mouse Look")]
public class MouseLook : MonoBehaviour {

	public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2, MouseXYController = 3}
	public RotationAxes axes = RotationAxes.MouseXAndY;
	public float sensitivityX = 15F;
	public float sensitivityY = 15F;

	public float minimumX = -360F;
	public float maximumX = 360F;

	public float minimumY = -60F;
	public float maximumY = 60F;

	float rotationY = 0F;
    bool enableRotate;

	private bool isMAC = false;
	private float rStickXAxis = 0f;
	private float rStickYAxis = 0f;

    public Slider MouseSensitivitySlider;

	void Update ()
	{

        if (MouseSensitivitySlider != null) {
            sensitivityX = MouseSensitivitySlider.value;
            sensitivityY = MouseSensitivitySlider.value;
        }



		SetControllerToOS();
		if (Input.GetMouseButton (1)) {
			if (axes == RotationAxes.MouseXAndY) {
				float rotationX = transform.localEulerAngles.y + Input.GetAxis ("Mouse X") * sensitivityX;
    			
				rotationY += Input.GetAxis ("Mouse Y") * sensitivityY;
				rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
    			
				transform.localEulerAngles = new Vector3 (-rotationY, rotationX, 0);
			} else if (axes == RotationAxes.MouseX) {
				transform.Rotate (0, Input.GetAxis ("Mouse X") * sensitivityX, 0);
			} else {
				rotationY += Input.GetAxis ("Mouse Y") * sensitivityY;
				rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
    			
				transform.localEulerAngles = new Vector3 (-rotationY, transform.localEulerAngles.y, 0);
			}
		} else if(Mathf.Abs(rStickXAxis)!=0 || Mathf.Abs(rStickYAxis)!=0) {
			
				float rotationX = transform.localEulerAngles.y + rStickXAxis * sensitivityX;

				rotationY += rStickYAxis * sensitivityY;
				rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);

				transform.localEulerAngles = new Vector3 (-rotationY, rotationX, 0);

		}
			
		
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    enableRotate = !enableRotate;
        //}
	}

    public void OverrideRotation(Vector3 rotation) {
        transform.localEulerAngles = new Vector3(rotation.x, rotation.y, 0);
    }
	
	void Start ()
	{
			// Make the rigid body not change rotation
		if (GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().freezeRotation = true;
		
	}

	private void SetControllerToOS() { //sets all the controller inputs based on the OS

		if (Application.platform == RuntimePlatform.OSXPlayer) {

			isMAC = true;

		}

		if (isMAC == true) {
			rStickXAxis = Input.GetAxis ("Mouse X Stick MAC");
			rStickYAxis = Input.GetAxis ("Mouse Y Stick MAC");
		} 
		else {
			rStickXAxis = Input.GetAxis ("Mouse X Stick");
			rStickYAxis = Input.GetAxis ("Mouse Y Stick");
		}
	}
}