using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Open_Side_Menu : MonoBehaviour {

	public GameObject Menu;
	public GameObject Floating_Text;
	public GameObject Main_Camera;
	public GameObject Main_Object;
	private Vector3 Normalized_Mouse_Location;
	private Vector3 Mouse_Location;
	private float Normalized_X;
	private float Normalized_Y;
	private GameObject[] allContextualMenus;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		if ((Main_Camera.transform.position - Main_Object.transform.position).magnitude > 1) {
		
			Menu.SetActive (false);

		}

		allContextualMenus = GameObject.FindGameObjectsWithTag ("Menu");

	}

	void OnMouseOver()
	{

			if ((Main_Camera.transform.position - Main_Object.transform.position).magnitude <= 1) {
			
				Mouse_Location = (Input.mousePosition);

				Normalized_X = (Mouse_Location [0] - ((Screen.width) / 2)) / 2.5f;

				Normalized_Y = (Mouse_Location [1] - ((Screen.height) / 2)) / 2.5f;

				Normalized_Mouse_Location = new Vector3 (Normalized_X, Normalized_Y, 0);

				if (Menu.activeSelf == false) {
			
					Floating_Text.SetActive (true);

					Floating_Text.transform.localPosition = Normalized_Mouse_Location;

				} else {
			
					Floating_Text.SetActive (false);
			
				}

				if (Input.GetMouseButtonDown (1)) {

					foreach (GameObject a in allContextualMenus) {
					
						a.SetActive (false);

					}

					Menu.SetActive (true);

				}

			}

			else {

				Floating_Text.SetActive (false);
				Menu.SetActive (false);

			}
	}

	void OnMouseExit(){
	
		Floating_Text.SetActive (false);

	}
}
