﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public enum tickStyle { endTicks, allTicks }

public class GraphCustomization : MonoBehaviour {

	public GameObject graphToCustomize;
	public GameObject graphBackground;
	public GameObject[] xTicks;
	public GameObject[] yTicks;
	public GameObject[] xLabels;
	public GameObject[] yLabels;
	public GameObject[] xGridLines;
	public GameObject[] yGridlines;
	public GameObject[] pointGraphics;
	public GameObject[] lineGraphics;
	public GameObject xAxis;
	public GameObject yAxis;

	[Range(.25f,2)]
	public float scaleCoefficient = 1;

	[Range(1,9)]
	public int xDivisions = 1;

	[Range(1,9)]
	public int yDivisions = 1;

	public List<Vector2> seriesData;
	public bool linesOn = true;
	public bool gridOn = true;
	public tickStyle xTickLabels = tickStyle.endTicks;
	public tickStyle yTickLabels = tickStyle.endTicks;


	[System.Serializable]
	public class GraphTitle { 
		public GameObject titleText; 
		public string label;
		[Range(10,26)]
		public int fontSize;
	}
	
	public GraphTitle titleOptions;

	[System.Serializable]
	public class GraphLabel { public GameObject labelText; 
		public string label;
		[Range(10,26)]
		public int fontSize; 
		public int lowerRange; 
		public int upperRange;
	}

	public GraphLabel xLabelOptions;
	public GraphLabel yLabelOptions;

	[System.Serializable]
	public class ColorSelection {

		public Color32 lineColor = new Color32 (255, 49, 49, 255);
		public Color32 pointColor = new Color32 (73, 255, 255, 255);
		public Color32 gridColor = new Color32 (144, 144, 144, 255);
		public Color32 textColor  = new Color32 (255, 255, 255, 255);
		public Color32 axisColor  = new Color32 (255, 255, 255, 255);
		public Color32 numberColor = new Color32 (255, 255, 255, 255);
		public Color32 backgroundColor = new Color32 (83, 83, 83, 255);


	}

	public ColorSelection colorOptions;

	// Use this for initialization
	void Start () {
	
	}

	void XTickMarksPlacement () {

		int xTicksLength = xTicks.Length;

		foreach (GameObject a in xTicks) {

			if (xTicksLength -1 > (8 - xDivisions)) {

				a.SetActive (true);

				a.transform.localPosition = new Vector3 (CalculateX (xTicksLength + xDivisions - 8), 3f);

			} else {

				a.SetActive (false);

			}

			xTicksLength = xTicksLength -1;

		}

	}

	void YTickMarksPlacement () {

		int yTicksLength = yTicks.Length;

		foreach (GameObject a in yTicks) {

			if (yTicksLength - 1 > (8 - yDivisions)) {

				a.SetActive (true);

				a.transform.localPosition = new Vector3 (3f, CalculateY (yTicksLength + yDivisions - 8) );

			} else {

				a.SetActive (false);

			}

			yTicksLength = yTicksLength -1;

		}

	}



	float CalculateX (int a) {

		float aFloat = (float)a - 1f;
		float subdivisionSize = 350 / (float)xDivisions;
		float xOffset = 175;
		float calculatedValue = subdivisionSize * aFloat - xOffset;
		//Debug.Log ("The calculated numbers are: " + aFloat + " " + subdivisionSize + " " + calculatedValue);
		return calculatedValue;

	}

	float CalculateY (int a) {

		float aFloat = (float)a - 1f;
		float subdivisionSize = 200 / (float)yDivisions;
		float yOffset = 100;
		float calculatedValue = subdivisionSize * aFloat - yOffset;
		return calculatedValue;

	}

	void scaleGraph () {

		graphToCustomize.transform.localScale = new Vector3 (1 * scaleCoefficient, 1 * scaleCoefficient, 1);

	}

	void setupTitle () {

		Text title = titleOptions.titleText.GetComponent<Text> ();

		title.text = titleOptions.label;
		title.fontSize = titleOptions.fontSize;

	}

	void setupXLabel () {

		Text label = xLabelOptions.labelText.GetComponent<Text> ();

		label.text = xLabelOptions.label;
		label.fontSize = xLabelOptions.fontSize;


	}

	void setupYLabel () {

		Text label = yLabelOptions.labelText.GetComponent<Text> ();

		label.text = yLabelOptions.label;
		label.fontSize = yLabelOptions.fontSize;

	}

	void setupXTickLabels () {

		Text endTick = xLabels [0].GetComponent<Text> ();
		Text originTick = xLabels [1].GetComponent<Text> ();

		endTick.text = "" + xLabelOptions.upperRange;
		originTick.text = "" + xLabelOptions.lowerRange;

		for (int i = 2; i <= xLabels.Length - 1; i++) {

			Text textToChange = xLabels [i].GetComponent<Text> ();
			textToChange.text = "" + (xLabelOptions.upperRange - ((float)(i - 1) * (xLabelOptions.upperRange - xLabelOptions.lowerRange) / (float)xDivisions )).ToString("F1");

		}
	}

	void setupYTickLabels () {

		Text endTick = yLabels [0].GetComponent<Text> ();
		Text originTick = yLabels [1].GetComponent<Text> ();

		endTick.text = "" + yLabelOptions.upperRange;
		originTick.text = "" + yLabelOptions.lowerRange;

		for (int i = 2; i <= yLabels.Length - 1; i++) {

			Text textToChange = yLabels [i].GetComponent<Text> ();
			textToChange.text = "" + (yLabelOptions.upperRange - ((float)(i - 1) * (yLabelOptions.upperRange - yLabelOptions.lowerRange) / (float)yDivisions )).ToString("F1");

		}
	
	}

	void setupXTickState () {

		if (xTickLabels == tickStyle.endTicks) {

			foreach (GameObject a in xLabels) {

				if (a == xLabels [0] || a == xLabels [1]) {

					a.SetActive (true);

				} else {

					a.SetActive (false);

				}



			}

		} else if (xTickLabels == tickStyle.allTicks) { 

			foreach (GameObject a in xLabels) {

				a.SetActive (true);

			}

		}	

	}

	void setupYTickState () {

		if (yTickLabels == tickStyle.endTicks) {

			foreach (GameObject a in yLabels) {

				if (a == yLabels [0] || a == yLabels [1]) {

					a.SetActive (true);

				} else {

					a.SetActive (false);

				}



			}

		} else if (yTickLabels == tickStyle.allTicks) { 

			foreach (GameObject a in yLabels) {

				a.SetActive (true);

			}

		}	

	}

	void setupPointsOnGraph () {

		for (int j = 0; j <= pointGraphics.Length - 1; j++) {

			if (j <= seriesData.Count - 1) {

				pointGraphics [j].SetActive (true);

			} else {

				pointGraphics [j].SetActive (false);

			}

		}

		for (int i = 0; i <= seriesData.Count - 1 ; i++) {

			Vector3 transformData = new Vector3 (calculateXCoordinate (seriesData[i].x) , calculateYCoordinate (seriesData[i].y) , 0f); 

			if (i <= pointGraphics.Length - 1) {

				pointGraphics [i].transform.localPosition = transformData;

			}
				
		}

	}

	float calculateXCoordinate (float x) {

		float xCalculated = (350f / (xLabelOptions.upperRange - xLabelOptions.lowerRange) * x) - 175f;
		return xCalculated;

	}

	float calculateYCoordinate (float y) {

		float yCalculated = (200f / (yLabelOptions.upperRange - yLabelOptions.lowerRange) * y) - 100f;
		return yCalculated;
	}

	void setupLineGraphics () {

		if (linesOn == true) {

			for (int i = 0; i <= lineGraphics.Length - 1; i++) {

				if (i <= seriesData.Count - 2) {

					Vector3 firstPoint = new Vector3 (pointGraphics [i].transform.localPosition.x, pointGraphics [i].transform.localPosition.y, 0f);
					Vector3 secondPoint = new Vector3 (pointGraphics [i + 1].transform.localPosition.x, pointGraphics [i + 1].transform.localPosition.y, 0f);

					if (seriesData [i] != seriesData [i + 1]) {
					
						lineGraphics [i].SetActive (true);
						lineGraphics [i].transform.localEulerAngles = (calculateZRotation (firstPoint, secondPoint));
						lineGraphics [i].transform.localPosition = calculateAverageTransform (firstPoint, secondPoint);
						lineGraphics [i].transform.localScale = calculateScale (firstPoint, secondPoint);

					} else {

						lineGraphics [i].SetActive (false);

					}

				} else {

					lineGraphics [i].SetActive (false);

				}

			}

		} else {

			foreach (GameObject a in lineGraphics) {

				a.SetActive (false);

			}
		}

	}

	Vector3 calculateZRotation ( Vector3 firstPointPosition , Vector3 secondPointPosition ) {

		float deltaY = Mathf.Abs((secondPointPosition.y) - (firstPointPosition.y));
		float deltaX = Mathf.Abs ((secondPointPosition.x) - (firstPointPosition.x));
		Vector3 angleToReturn = new Vector3 (0f, 0f, 0f);
		float zAngle = 0f;
		float deltaRatio = deltaX / deltaY;
		float inverseRatio = deltaY / deltaX;
		float zAngleRad1 = Mathf.Atan (deltaRatio);
		float zAngleRad2 = Mathf.Atan (inverseRatio);

		if (firstPointPosition.y >= secondPointPosition.y) {

			zAngle = Mathf.Rad2Deg * (zAngleRad1) - 90f;

		}

		if (firstPointPosition.y < secondPointPosition.y) {

			zAngle = Mathf.Rad2Deg * (zAngleRad2);

		}

		angleToReturn = new Vector3 (0f, 0f, zAngle);

		return angleToReturn;

	}

	Vector3 calculateAverageTransform ( Vector3 firstPointPosition , Vector3 secondPointPosition ) {

		Vector3 positionToReturn = new Vector3 (0f, 0f, 0f);

		float x1 = firstPointPosition.x;
		float x2 = secondPointPosition.x;
		float y1 = firstPointPosition.y;
		float y2 = secondPointPosition.y;

		float xAverage = (x1 + x2) / 2f;
		float yAverage = (y1 + y2) / 2f;

		positionToReturn = new Vector3 (xAverage, yAverage, 0f);

		return positionToReturn;

	}

	Vector3 calculateScale ( Vector3 firstPointPosition , Vector3 secondPointPosition ) {

		Vector3 scaleToReturn = new Vector3 (0f, 0f, 0f);
		float deltaY = Mathf.Abs(secondPointPosition.y - firstPointPosition.y);
		float deltaX = Mathf.Abs (secondPointPosition.x - firstPointPosition.x);
		float distanceMeasured = Mathf.Sqrt ((Mathf.Pow((deltaX), 2) + Mathf.Pow((deltaY), 2)));
		float scaleModifier = distanceMeasured / 100f;

		scaleToReturn = new Vector3 (scaleModifier, 1f, 1f);

		return scaleToReturn;

	}

	void setGridVisibility () {

		if (gridOn == true) {

			foreach (GameObject a in xGridLines) {

				a.SetActive (true);

			}

			foreach (GameObject b in yGridlines) {

				b.SetActive (true);

			}

		} else {

			foreach (GameObject a in xGridLines) {

				a.SetActive (false);

			}

			foreach (GameObject b in yGridlines) {

				b.SetActive (false);

			}

		}

	}

	void setColors () {

		titleOptions.titleText.GetComponent<Text> ().color = colorOptions.textColor;
		xLabelOptions.labelText.GetComponent<Text> ().color = colorOptions.textColor;
		yLabelOptions.labelText.GetComponent<Text> ().color = colorOptions.textColor;

		for (int i = 0; i <= xTicks.Length - 1; i++) {

			xTicks [i].GetComponent<Image> ().color = colorOptions.axisColor;

		}



		for (int i = 0; i <= yTicks.Length - 1; i++) {

			yTicks [i].GetComponent<Image> ().color = colorOptions.axisColor;

		}

		GameObject[] endTicks = GameObject.FindGameObjectsWithTag ("EndTicks");

		for (int i = 0; i <= endTicks.Length - 1; i++) {

			endTicks [i].GetComponent<Image> ().color = colorOptions.axisColor;

		}

		xAxis.GetComponent<RawImage> ().color = colorOptions.axisColor;

		GameObject[] labelAmounts = GameObject.FindGameObjectsWithTag ("Label");

		for (int i = 0; i <= labelAmounts.Length - 1; i++) {

			labelAmounts [i].GetComponent<Text> ().color = colorOptions.numberColor;

		}

		graphBackground.GetComponent<Image> ().color = colorOptions.backgroundColor;

		for (int i = 0; i <= xGridLines.Length - 1; i++) {

			xGridLines [i].GetComponent<Image> ().color = colorOptions.gridColor;

		}

		for (int i = 0; i <= yGridlines.Length - 1; i++) {

			yGridlines [i].GetComponent<RawImage> ().color = colorOptions.gridColor;

		}



	}
		

	[ExecuteInEditMode]
	void OnValidate () {

		scaleGraph ();
		XTickMarksPlacement ();
		YTickMarksPlacement ();
		setupTitle ();
		setupXLabel ();
		setupYLabel ();
		setupXTickState ();
		setupXTickLabels ();
		setupYTickState ();
		setupYTickLabels ();
		setupPointsOnGraph ();
		setupLineGraphics ();
		setGridVisibility ();
		setColors ();

	}



}
