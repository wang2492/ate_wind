﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetActive_S : MonoBehaviour {

	public GameObject subMenu;

	public void setactive(){
		if (subMenu.activeSelf == true) {
			subMenu.SetActive (false);
		} else {
			subMenu.SetActive (true);
		}
	}

	void Update()
	{    
		if (subMenu.activeSelf == true) {		
			this.gameObject.GetComponent<Image> ().color = Color.gray;
		} else {
			this.gameObject.GetComponent<Image> ().color = Color.white;
		}

	}


}