﻿using UnityEngine;
using System.Collections;

public class ChangeMaterial : MonoBehaviour {


    public bool pwrison;

    //public Material White;
    //public Material Yellow;
    //public Material Red;

    public Material[] color;

 //   Renderer rend;

    // Use this for initialization
    void Start () {

     
  //     rend = GetComponent<Renderer>();
           
    }

    // Update is called once per frame
    void Update()
    {


        pwrison = GameObject.Find("Scriptholder").GetComponent<Changelight>().powerison;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;


        if (Physics.Raycast(ray, out hit, 8))
        {

            if (hit.collider.name == "PowerSwitch" )
            // gameObject.name == "PWRLight"
            {

                if (pwrison == true)
                {
                    GameObject.Find("PWRLight").GetComponent<Renderer>().material = color[1];
                    if (GameObject.Find("Scriptholder").GetComponent<Changelight>().doorisopen == false)
                     {
                        GameObject.Find("DoorLight").GetComponent<Renderer>().material = color[1];
                    }
                    else { GameObject.Find("DoorLight").GetComponent<Renderer>().material = color[0];}
                }
                else {
                    GameObject.Find("PWRLight").GetComponent<Renderer>().material = color[0];
                    GameObject.Find("DoorLight").GetComponent<Renderer>().material = color[0];
                }
            }

            if (hit.collider.name == "Door")
            {                

                if (GameObject.Find("Scriptholder").GetComponent<Changelight>().doorisopen == false && pwrison == true )
                {
                    GameObject.Find("DoorLight").GetComponent<Renderer>().material = color[1];
                }
                else { GameObject.Find("DoorLight").GetComponent<Renderer>().material = color[0]; }
            }

            if (hit.collider.name == "LightSwitch" && pwrison == true)
            {
         
                if (GameObject.Find("Scriptholder").GetComponent<Changelight>().switchison == true )
                {
                    GameObject.Find("LSLight").GetComponent<Renderer>().material = color[1];
                }
                else { GameObject.Find("LSLight").GetComponent<Renderer>().material = color[0]; }
            }

            if (pwrison == true && GameObject.Find("Scriptholder").GetComponent<Changelight>().runlight == true ) {
                GameObject.Find("RUNLight").GetComponent<Renderer>().material = color[1];
            }else
            {
                GameObject.Find("RUNLight").GetComponent<Renderer>().material = color[0];
            }

        }


    }
}
