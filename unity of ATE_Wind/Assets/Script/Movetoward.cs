﻿using UnityEngine;
using System.Collections;

public class Movetoward : MonoBehaviour {
    public Transform NacelleEndPosition;
    public Transform HubEndPosition;
    public Transform BladesEndPosition;
    public Transform TowerEndPosition;
    public float Speed;
    public GameObject MainCamera;
    private bool MoveNacelle;
    private bool MoveHub;
    private bool MoveBlades;
    private bool MoveTower;

    private Vector3 currentPosition;
    private Quaternion currentAngle;

    void Start()
    {
        //startTime = Time.deltaTime;
        MoveNacelle = false;
        MoveHub = false;
        MoveBlades = false;
        MoveTower = false;
    }
	void Update () {

        if (MoveNacelle)
        {
            float distance = Vector3.Distance(currentPosition, NacelleEndPosition.position);
            float angle = Quaternion.Angle(currentAngle, NacelleEndPosition.rotation);
            float rotateSpeed = (angle / distance) * Speed;

            float journeyLength = Vector3.Distance(MainCamera.transform.position, NacelleEndPosition.position);
            float angleCovered = Quaternion.Angle(MainCamera.transform.rotation, NacelleEndPosition.rotation);

            if (journeyLength < 0.05f && angleCovered < 0.05f)
            {
                MoveNacelle = false;
            }
            else
            {
                if (journeyLength > 25f)
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, NacelleEndPosition.position, 5 * Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, NacelleEndPosition.rotation, 5 * rotateSpeed * Time.deltaTime);
                }
                else
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, NacelleEndPosition.position, Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, NacelleEndPosition.rotation, rotateSpeed * Time.deltaTime);
                }
            }

            if (Input.anyKeyDown)
            {
                MoveNacelle = false;
            }
        }
        if (MoveHub)
        {
            float distance = Vector3.Distance(currentPosition, HubEndPosition.position);
            float angle = Quaternion.Angle(currentAngle, HubEndPosition.rotation);
            float rotateSpeed = (angle / distance) * Speed;

            float journeyLength = Vector3.Distance(MainCamera.transform.position, HubEndPosition.position);
            float angleCovered = Quaternion.Angle(MainCamera.transform.rotation, HubEndPosition.rotation);

            if (journeyLength < 0.05f && angleCovered < 0.05f)
            {
                MoveHub = false;
            }
            else
            {
                if (journeyLength > 25f)
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, HubEndPosition.position, 5 * Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, HubEndPosition.rotation, 5 * rotateSpeed * Time.deltaTime);
                }
                else
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, HubEndPosition.position, Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, HubEndPosition.rotation, rotateSpeed * Time.deltaTime);
                }
            }

            if (Input.anyKeyDown)
            {
                MoveHub = false;
            }
        }
        if (MoveBlades)
        {
            float distance = Vector3.Distance(currentPosition, BladesEndPosition.position);
            float angle = Quaternion.Angle(currentAngle, BladesEndPosition.rotation);
            float rotateSpeed = (angle / distance) * Speed;

            float journeyLength = Vector3.Distance(MainCamera.transform.position, BladesEndPosition.position);
            float angleCovered = Quaternion.Angle(MainCamera.transform.rotation, BladesEndPosition.rotation);

            if (journeyLength < 0.05f && angleCovered < 0.05f)
            {
                MoveBlades = false;
            }
            else
            {
                if (journeyLength > 25f)
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, BladesEndPosition.position, 5 * Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, BladesEndPosition.rotation, 5 * rotateSpeed * Time.deltaTime);
                }
                else
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, BladesEndPosition.position, Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, BladesEndPosition.rotation, rotateSpeed * Time.deltaTime);
                }
            }

            if (Input.anyKeyDown)
            {
                MoveBlades = false;
            }
        }
        if (MoveTower)
        {
            float distance = Vector3.Distance(currentPosition, TowerEndPosition.position);
            float angle = Quaternion.Angle(currentAngle, TowerEndPosition.rotation);
            float rotateSpeed = (angle / distance) * Speed;

            float journeyLength = Vector3.Distance(MainCamera.transform.position, TowerEndPosition.position);
            float angleCovered = Quaternion.Angle(MainCamera.transform.rotation, TowerEndPosition.rotation);

            if (journeyLength < 0.05f && angleCovered < 0.05f)
            {
                MoveTower = false;
            }
            else
            {
                if (journeyLength > 25f)
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, TowerEndPosition.position, 5 * Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, TowerEndPosition.rotation, 5 * rotateSpeed * Time.deltaTime);
                }
                else
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, TowerEndPosition.position, Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, TowerEndPosition.rotation, rotateSpeed * Time.deltaTime);
                }
            }

            if (Input.anyKeyDown)
            {
                MoveTower = false;
            }
        }

    }
    public void MovetoNacelle()
    {
        MoveNacelle = true;
        currentPosition = MainCamera.transform.position;
        currentAngle = MainCamera.transform.rotation;
    }
    public void MovetoHub()
    {
        MoveHub = true;
        currentPosition = MainCamera.transform.position;
        currentAngle = MainCamera.transform.rotation;
    }
    public void MovetoBlades()
    {
        MoveBlades = true;
        currentPosition = MainCamera.transform.position;
        currentAngle = MainCamera.transform.rotation;
    }
    public void MovetoTower()
    {
        MoveTower = true;
        currentPosition = MainCamera.transform.position;
        currentAngle = MainCamera.transform.rotation;
    }
}
