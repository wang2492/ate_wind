using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class measurementAssignment : MonoBehaviour {
	
	public GameObject measurementReadoutMenu;
	public Text measurementReadout;
	public Text[] probeSelected;
	public Text unitMeasurementText;
    public Text acOrDc;

	private float highMeasurementValue = 0f;
	private float highMeasurementFixedValue = 0f;
	private bool highMeasurementIsTaken = false;
	private bool highMeasurementButtonPressed = false;
	private bool highMeasurementLocationPressed = false;
	private float lowMeasurementValue = 0f;
	private float lowMeasurementFixedValue = 0f;
	private bool lowMeasurementIsTaken = false;
	private bool lowMeasurementButtonPressed = false;
	private bool lowMeasurementLocationPressed = false;
	private bool newLowMeasurementIsTaken = false;
	private bool newHighMeasurementIsTaken = false;
	private bool isAC = false;
	private bool lowIsAC = false;
	private bool highIsAC = false;
    private int acPhase = 1;
    private int highPhase = 1;
    private int lowPhase = 1;
	private bool isDisconnected = false;
    private bool isNeutral = false;
    private bool isSinglePhase =false;
    private bool lowSinglePhase = false;
    private bool highSinglePhase = false;

    private float lineToLineFactor = Mathf.Sqrt(3f);
    private float singlePhaseFactor = 1;

	private float ampageValue = 0;
	private float ampageFixedValue = 0;

	private float resistanceValue = 0;
	private float resistanceFixedValue = 0;

	private int highMeasurementId = 0;
	private int lowMeasurementId = 0;

	private int objectNumber = 0;
	private int componentNumber = 0;
	private float overallMeasurement;
	private bool voltageOn = true;
	private bool ampageOn = false;
	private bool resistanceOn = false;
	private bool replaceFuseOn = false;

    //variable used for disconnecting AC nodes on manual mode
    public bool isOnManual = false;

    //variables used for adjusting the randomness of the measurements
    public float percentageOfRandomness = 2.5f;
    private float RandomnessValue = 10f; //this value will be calculated using the current value of meter and PoR
    private float randomnessIntervalValue = .1f;
    private float randomSteadyState;
    private bool maxedInterval = false;

	// Use this for initialization
	void Start () {

        measurementReadout.text = "0.00";
        StartCoroutine("RandomnessGeneration");

	}
	
	// Update is called once per frame
	void Update () {

		//newLowMeasurementIsTaken = false;
		//newHighMeasurementIsTaken = false;
		UpdateReadingParameters ();
		Reading ();
        AssignHighLowText(probeSelected);

	}

	public void LocationIsDisconnected () {

		isDisconnected = true;

	}

    public void LocationPhase(int a) {

        acPhase = a;

    }

    public void IsSinglePhase()
    {

        isSinglePhase = true;

    }

    public void IsNeutral()
    {

        isNeutral = true;

    }

    private void SetHighLocationPhase() {

        if (isNeutral)
        {

            highPhase = 0;

        } else
        {

            highPhase = acPhase;

        }

        highSinglePhase = isSinglePhase;

        isNeutral = false;
        isSinglePhase = false;
        

    }

    private void SetLowLocationPhase()
    {

        if (isNeutral)
        {

            lowPhase = 0;

        }
        else
        {

            lowPhase = acPhase;

        }

        lowSinglePhase = isSinglePhase;

        isNeutral = false;
        isSinglePhase = false;


    }

    public void LocationIsAC () {

		isAC = true;

        isDisconnected = isOnManual;

	}

	private void HighLocationACCheck () {

		if (isAC == true) {
			
			highIsAC = true;
			isAC = false;

		}


	}

	private void LowLocationACCheck () {

			if (isAC == true) {

				lowIsAC = true;
				isAC = false;

			}

	}

	public float ReturnMeasurementValue () {

		return highMeasurementValue;

	}

	public void HighMeasurementButtonToggle (){
	
		highMeasurementButtonPressed = !highMeasurementButtonPressed;
		lowMeasurementButtonPressed = false;

	}

	public void HighMeasurementLocationButtonToggle (){
	
		if (highMeasurementButtonPressed == true) {
			
			highMeasurementLocationPressed = !highMeasurementLocationPressed;
		
		}
	}

	public void LowMeasurementButtonToggle (){

		lowMeasurementButtonPressed = !lowMeasurementButtonPressed;
		highMeasurementButtonPressed = false;

	}

	public void LowMeasurementLocationButtonToggle (){

		if (lowMeasurementButtonPressed == true) {
		
			lowMeasurementLocationPressed = !lowMeasurementLocationPressed;
		
		}
	}

	public bool GetNewHighMeasurementTakenBool () {

		return newHighMeasurementIsTaken;

	}

	public bool GetNewLowMeasurementTakenBool () {

		return newLowMeasurementIsTaken;

	}

	public bool GetHighMeasurementTakenBool () {

		return highMeasurementIsTaken;

	}

	public bool GetLowMeasurementTakenBool () {

		return lowMeasurementIsTaken;

	}

	public bool GetHighMeasurementBool () {

		return highMeasurementButtonPressed;

	}

	public bool GetLowMeasurementBool () {

		return lowMeasurementButtonPressed;

	}

	public bool GetHighLocationPressed () {

		return highMeasurementLocationPressed;

	}

	public bool GetLowLocationPressed () {

		return lowMeasurementLocationPressed;

	}

	public void ResetNewMeasurements () {

		newHighMeasurementIsTaken = false;
		newLowMeasurementIsTaken = false;
		isAC = false;

	}

	public void SetObjectNumber (int a) {

		objectNumber = a;

	}

	public void SetComponentNumber (int b) {
	
		componentNumber = b;
	
	}

	public int GetObjectNumber () {
	
		return objectNumber;
	
	}

	public int GetComponentNumber () {
	
		return componentNumber;
	
	}

	public void SetHighMeasurementValue (float a){
	
		highMeasurementValue = a;
	
	}

	public void SetLowMeasurementValue (float b){
	
		lowMeasurementValue = b;
	
	}

	public void SetHighMeasurementID (int b){

		highMeasurementId = b;

	}

	public void SetLowMeasurementID (int b){

		lowMeasurementId = b;

	}

	public void SetAmpage (float b) {

		ampageValue = b;

	}

	public void SetResistance (float b) {

		resistanceValue = b;

	}

	public void ResetReadings () {

		highMeasurementIsTaken = false;
		lowMeasurementIsTaken = false;

	}

	public float VoltageReading () {

		overallMeasurement = highMeasurementFixedValue - lowMeasurementFixedValue;
		return overallMeasurement;

	}


	public void AssignHighLowText (Text[] a) {
	
		if (highMeasurementButtonPressed == true) {

            foreach (Text b in a)
            {

                b.text = "High";

            }

		} else if (lowMeasurementButtonPressed == true) {

            foreach (Text b in a)
            {

                b.text = "Low";

            }

		} else {

            foreach (Text b in a)
            {

                b.text = "None";

            }

		}
	
	}

	private void Reading () {
		
		if (voltageOn == true) {
			
			if (highMeasurementButtonPressed == true & highMeasurementLocationPressed == true) {

				if (highMeasurementIsTaken == true) {

					newHighMeasurementIsTaken = true;

				}

			
					
				HighLocationACCheck ();
                SetHighLocationPhase();
				highMeasurementIsTaken = true;
				highMeasurementFixedValue = highMeasurementValue;
				highMeasurementButtonPressed = false;
				highMeasurementLocationPressed = false;
				Debug.Log ("highMeasurement value: " + highMeasurementFixedValue);

			}

			if (lowMeasurementButtonPressed == true & lowMeasurementLocationPressed == true) {

				if (lowMeasurementIsTaken == true) {

					newLowMeasurementIsTaken = true;

				}


					
				LowLocationACCheck ();
                SetLowLocationPhase();
				lowMeasurementIsTaken = true;
				lowMeasurementFixedValue = lowMeasurementValue;
				lowMeasurementButtonPressed = false;
				lowMeasurementLocationPressed = false;
				Debug.Log ("lowMeasurement value: " + lowMeasurementFixedValue);

			}

			if (highMeasurementIsTaken == true & lowMeasurementIsTaken == true) {


				if (isDisconnected == true) {

					overallMeasurement = 0f;
                    string a = overallMeasurement.ToString("F");
                    measurementReadout.text = a;
					measurementReadoutMenu.SetActive (true);
					highMeasurementIsTaken = false;
					lowMeasurementIsTaken = false;
					unitMeasurementText.text = "Volts";
					Debug.Log (isDisconnected);

				} else if (highIsAC == true & lowIsAC == true) {

                    acOrDc.text = "AC";

                    if ((highPhase == 0) || (lowPhase == 0))
                    {

                        if (highPhase == 0)
                        {
                            if (lowPhase == 0)
                            {

                                overallMeasurement = 0;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }

                            else if (lowSinglePhase)

                            {

                                overallMeasurement = lowMeasurementFixedValue / singlePhaseFactor;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }
                            else
                            {

                                overallMeasurement = lowMeasurementFixedValue / lineToLineFactor;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }

                        }

                        if (lowPhase == 0)
                        {
                            if (highPhase == 0)
                            {

                                overallMeasurement = 0;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }
                            else if (highSinglePhase)
                            {

                                overallMeasurement = highMeasurementFixedValue / singlePhaseFactor;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }
                            else
                            {

                                overallMeasurement = highMeasurementFixedValue / lineToLineFactor;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }
                        }

                    }
                    else if (highPhase != lowPhase)
                    {
                        {

                            if (highMeasurementFixedValue == lowMeasurementFixedValue)
                            {

                                overallMeasurement = highMeasurementFixedValue;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }

                        }
                    } else if (highPhase == lowPhase)
                    {

                        measurementReadout.text = "Invalid";
                        measurementReadoutMenu.SetActive(true);
                        highMeasurementIsTaken = false;
                        lowMeasurementIsTaken = false;
                        unitMeasurementText.text = " ";

                    }


				} else if ((highIsAC == true & lowIsAC == false) || (highIsAC == false & lowIsAC == true)) {

							measurementReadout.text = "Invalid";
							measurementReadoutMenu.SetActive (true);
							highMeasurementIsTaken = false;
							lowMeasurementIsTaken = false;
							unitMeasurementText.text = " ";
					        Debug.Log (overallMeasurement);

				} else {

                    acOrDc.text = "DC";
					overallMeasurement = VoltageReading ();
                    string a = overallMeasurement.ToString("F");
                    measurementReadout.text = a;
					measurementReadoutMenu.SetActive (true);
					highMeasurementIsTaken = false;
					lowMeasurementIsTaken = false;
					unitMeasurementText.text = "Volts";
					Debug.Log (overallMeasurement);

				}

				highIsAC = false;
				lowIsAC = false;
				isDisconnected = false;

                if (overallMeasurement >= 10f)
                {

                    RandomnessValue = overallMeasurement;

                }
                else
                {

                    RandomnessValue = 10f;

                }

                randomnessIntervalValue = .1f;
                maxedInterval = false;

                //Debug.Log("Randomness Value = " + RandomnessValue);

            }
				
		}



		

		if (ampageOn == true) {

			if (highMeasurementButtonPressed == true & highMeasurementLocationPressed == true) {

				if (highMeasurementIsTaken == true) {

					newHighMeasurementIsTaken = true;

				}
					
				highMeasurementIsTaken = true;
				ampageFixedValue = ampageValue;
				highMeasurementButtonPressed = false;
				highMeasurementLocationPressed = false;
								 
			}

			if (lowMeasurementButtonPressed == true & lowMeasurementLocationPressed == true) {

				if (lowMeasurementIsTaken == true) {

					newLowMeasurementIsTaken = true;

				}

				lowMeasurementIsTaken = true;
				lowMeasurementButtonPressed = false;
				lowMeasurementLocationPressed = false;

			}

			if (highMeasurementIsTaken == true & lowMeasurementIsTaken == true) {

				if (highMeasurementId == lowMeasurementId) {
					
					overallMeasurement = ampageFixedValue;
                    string a = overallMeasurement.ToString("F");
                    measurementReadout.text = a;
					measurementReadoutMenu.SetActive (true);
					highMeasurementIsTaken = false;
					lowMeasurementIsTaken = false;
					unitMeasurementText.text = "Amps";

				} else {

					measurementReadout.text = "Invalid";
					measurementReadoutMenu.SetActive (true);
					highMeasurementIsTaken = false;
					lowMeasurementIsTaken = false;
					unitMeasurementText.text = " ";

				}
					
			}

		}

		if (resistanceOn == true) {

			if (highMeasurementButtonPressed == true & highMeasurementLocationPressed == true) {

				if (highMeasurementIsTaken == true) {

					newHighMeasurementIsTaken = true;

				}

				highMeasurementIsTaken = true;
				resistanceFixedValue = resistanceValue;
				highMeasurementButtonPressed = false;
				highMeasurementLocationPressed = false;

			}

			if (lowMeasurementButtonPressed == true & lowMeasurementLocationPressed == true) {

				if (lowMeasurementIsTaken == true) {

					newLowMeasurementIsTaken = true;

				}

				lowMeasurementIsTaken = true;
				lowMeasurementButtonPressed = false;
				lowMeasurementLocationPressed = false;

			}

			if (highMeasurementIsTaken == true & lowMeasurementIsTaken == true) {

				if (highMeasurementId == lowMeasurementId) {

					overallMeasurement = resistanceFixedValue;
					measurementReadout.text = System.Convert.ToString (overallMeasurement);
					measurementReadoutMenu.SetActive (true);
					highMeasurementIsTaken = false;
					lowMeasurementIsTaken = false;
					unitMeasurementText.text = "Ohms";

				} else {

					measurementReadout.text = "Invalid";
					measurementReadoutMenu.SetActive (true);
					highMeasurementIsTaken = false;
					lowMeasurementIsTaken = false;
					unitMeasurementText.text = " ";

				}
					
			}

		}
	}
								

	private void UpdateReadingParameters () {

		GameObject scriptHolder = GameObject.FindGameObjectWithTag ("ScriptHolder");

		MeasurementModeSelection particularScript = scriptHolder.GetComponent<MeasurementModeSelection> ();

		voltageOn = particularScript.GetVoltageBool ();

		ampageOn = particularScript.GetAmpageBool ();

		resistanceOn = particularScript.GetResistanceBool ();

	}

	public void SettingIdOnClick (int a) {

		if (highMeasurementButtonPressed == true) {

			highMeasurementId = a;

		}

		if (lowMeasurementButtonPressed == true) {

			lowMeasurementId = a;

		}
	}

	public void ReplaceFuse(){
		replaceFuseOn = true;
	}

	public void DCMeasurementAssignment(float a) {

		SetHighMeasurementValue (a);
		SetLowMeasurementValue (a);
		HighMeasurementLocationButtonToggle ();
		LowMeasurementLocationButtonToggle ();


	}



    public void SetManualMode(bool value) {
        Debug.Log("setting manual mode to: " + value);
        isOnManual = value;
    }

    IEnumerator RandomnessGeneration ()
    {

        while (true)
        {

            yield return new WaitForSeconds(randomnessIntervalValue);

            if (maxedInterval == false)
            {
                float coroutineRandomnessValue = RandomnessValue;
                float calculatedRandomness = ((.01f) * (Random.value * percentageOfRandomness) * (coroutineRandomnessValue)) - ((.01f) * (.5f) * (percentageOfRandomness * coroutineRandomnessValue)) + overallMeasurement;
                randomSteadyState = calculatedRandomness;
                string temporaryMeasurement = calculatedRandomness.ToString("F");
                measurementReadout.text = temporaryMeasurement;

                
                if (randomnessIntervalValue <= 1.5f)
                {

                    randomnessIntervalValue = randomnessIntervalValue * 2;

                }
                else
                {

                    maxedInterval = true;
                    randomnessIntervalValue = .1f;

                }

            }
            else
            {

                measurementReadout.text = randomSteadyState.ToString("F");

            }


        }
        


    }


}
