﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainDistanceController : MonoBehaviour {

    public int Distance=1000;

    [Range(0f,1f)]
    public float density = 1f;

    public GameObject cam;

	void Update () {
        GetComponent<Terrain>().detailObjectDistance = Distance;
        GetComponent<Terrain>().detailObjectDensity = density;

	}
	
}
