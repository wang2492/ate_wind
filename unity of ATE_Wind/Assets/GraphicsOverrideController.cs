﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.PostProcessing;

public class GraphicsOverrideController : MonoBehaviour {

    public Dropdown presetDrowdown;
    public GraphicsPreset[] presets;

    public Slider distanceSlider;
    public Slider densitySlider;
    public Slider cameraRangeSlider;

    public Toggle ppeToggle;
    public Toggle treeToggle;

    



    public static int grassDrawDistance;
    public static float grassDensity;
    public static bool usePostProcessingEffects;
    public static bool renderTrees;
    public static float cameraRange;

    void Start() {
        DontDestroyOnLoad(gameObject);
    }

    void Update() {
        if(Input.GetKeyDown(KeyCode.Keypad5)) {
            Debug.Log(grassDrawDistance + "," + grassDensity + "," + usePostProcessingEffects);
        }
    }

    public void Submit() {
        grassDrawDistance = (int)distanceSlider.value;
        grassDensity = densitySlider.value;
        usePostProcessingEffects = ppeToggle.isOn;
        renderTrees = treeToggle.isOn;
        cameraRange = cameraRangeSlider.value;

    }


    public void ApplyPreset(int index) {
        GraphicsPreset p = presets[index];
        distanceSlider.value = p.grassDistance;
        densitySlider.value = p.grassDensity;
        ppeToggle.isOn = p.usePPE;
        treeToggle.isOn = p.RenderTrees;
        cameraRangeSlider.value = p.CameraRange;
    }

    public static void ApplyCustomSettings(int newGrassDistance, float newGrassDensity, bool newPPE, bool newTrees, float newCamRange) {
        grassDrawDistance=newGrassDistance;
        grassDensity=newGrassDensity;
        usePostProcessingEffects=newPPE;
        renderTrees=newTrees;
        cameraRange=newCamRange;
}

}
